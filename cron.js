module.exports = ({ cron }, service, { Cache, logger, db }) => {
  cron.schedule("*/1 * * * *", async () => {
    logger.info("Performing Cancelling expired transaction");
    await db.raw(
      "update trn_transaction set `status` = 400 where expired_at < now() and `status` not in (300,400)"
    );
    logger.info("Succesfully Cancelling expired transaction");

    logger.info("Performing Expired ticket");
    await db.raw(
      "update trn_ticket set `status` = 400 where use_before < now() and `status` not in (300)"
    );
    logger.info("Succesfully Expired ticket");
  });
};

// Update with your config settings.
module.exports = {
  development: {
    client: "mysql",
    connection: {
      host: process.env.DB_URL,
      port: process.env.DB_PORT,
      database: process.env.DB_NAME,
      user: process.env.DB_USER,
      password: process.env.DB_PASS
    },
    pool: {
      min: 0,
      max: 10
    }
  },
  production: {
    client: "mysql",
    connection: {
      host: process.env.DB_URL,
      port: process.env.DB_PORT,
      database: process.env.DB_NAME,
      user: process.env.DB_USER,
      password: process.env.DB_PASS
    },
    pool: {
      min: 2,
      max: 10
    }
  }
};

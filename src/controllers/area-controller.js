const { Router } = require("express");
const index = Router();

module.exports = ({
  app: { validate, Joi },
  service: { AreaService },
  utils: { BuildResponse }
}) => {
  index.get("/", async (req, res, next) => {
    try {
      let data = await AreaService.findAll();
      BuildResponse(req, res, {
        status_code: 200,
        data: data
      });
    } catch (err) {
      next(err);
    }
  });
  index.post(
    "/",
    [
      validate({
        body: {
          latitude: Joi.required(),
          longitude: Joi.required()
        }
      })
    ],
    async (req, res, next) => {
      try {
        const { latitude, longitude } = req.body;
        let data = await AreaService.findAllWithLatLing(latitude, longitude);
        BuildResponse(req, res, {
          status_code: 200,
          data: data
        });
      } catch (err) {
        next(err);
      }
    }
  );
  return index;
};

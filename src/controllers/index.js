const user_controller = require("./users-controller");
const area_controller = require("./area-controller");
const route_controller = require("./route-controller");
const schedule_controller = require("./schedule-controller");
const voucher_controller = require("./voucher-controller");
const subscription_controller = require("./subscription-controller");
const transaction_controller = require("./transaction-controller");
const product_controller = require("./product-controller");
const payment_controller = require("./payment-controller");
const midtrans_controller = require("./midtrans-controller");
const ticket_controller = require("./ticket-controller");
module.exports = {
  voucher_controller,
  user_controller,
  area_controller,
  route_controller,
  subscription_controller,
  schedule_controller,
  transaction_controller,
  product_controller,
  payment_controller,
  midtrans_controller,
  ticket_controller
};

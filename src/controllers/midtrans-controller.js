const sha512 = require("js-sha512").sha512;
const { Router } = require("express");
const route = Router();

module.exports = ({
  app: { validate, Joi },
  service: { TransactionService },
  utils: { db, logger, encode_base64 }
}) => {
  route.post("/notification-payment", async (req, res, next) => {
    try {
      const {
        transaction_id,
        payment_type,
        transaction_status,
        fraud_status,
        order_id,
        status_code,
        gross_amount,
        signature_key
      } = req.body;
      await db("log_midtrans").insert({
        transaction_id: transaction_id,
        type: payment_type,
        data: encode_base64(req.body)
      });
      if (process.env.NODE_ENV == "production") {
        if (
          sha512(
            order_id +
              status_code +
              gross_amount +
              process.env.MIDTRANS_SERVER_KEY
          ) !== signature_key
        ) {
          res
            .send("Invalid Signature")
            .status(200)
            .end();
          return;
        }
      }
      logger.info(
        __dirname +
          `Transaction notification received. Order ID: ${order_id}. Transaction status: ${transaction_status}. Fraud status: ${fraud_status}`
      );
      if (transaction_status == "capture") {
        if (fraud_status == "challenge") {
          let transaction = await TransactionService.findById(order_id);
          if (transaction.status == "200") {
            TransactionService.challenge_payment({
              order_id: order_id
            });
          }
          res.status(200).end();
          // TODO set transaction status on your databaase to 'challenge'
        } else if (fraud_status == "accept") {
          let transaction = await TransactionService.findById(order_id);
          if (transaction.status == "200") {
            await TransactionService.success_payment({
              order_id: order_id
            });
            res.status(200).end();
          }
          res.status(200).end();
        }
      } else if (
        transaction_status == "cancel" ||
        transaction_status == "deny" ||
        transaction_status == "expire"
      ) {
        let transaction = await TransactionService.findById(order_id);
        if (transaction.status == "200") {
          TransactionService.failed_payment({
            order_id: order_id
          });
        }
        res.status(200).end();
        // TODO set transaction status on your databaase to 'failure'
      } else if (transaction_status == "pending") {
        res.status(200).end();
        // TODO set transaction status on your databaase to 'pending' / waiting payment
      }
    } catch (e) {
      if (!e instanceof FailedTaskExecution) {
        logger.error(__dirname + e.message);
      }
      next(e);
    }
  });
  return route;
};

const { Router } = require("express");
const index = Router();

module.exports = ({
  app: { validate, Joi },
  service: { PaymentService },
  utils: { BuildResponse }
}) => {
  index.get("/type", async (req, res, next) => {
    try {
      let data = await PaymentService.findAllPaymentType();
      BuildResponse(req, res, {
        status_code: 200,
        data: data
      });
    } catch (err) {
      next(err);
    }
  });
  return index;
};

const { Router } = require("express");
const index = Router();

module.exports = ({
  app: { validate, Joi },
  service: { ProductService },
  utils: { BuildResponse }
}) => {
  index.get("/", async (req, res, next) => {
    try {
      let data = await ProductService.findAll();
      BuildResponse(req, res, {
        status_code: 200,
        data: data
      });
    } catch (err) {
      next(err);
    }
  });
  index.get("/:product_id", async (req, res, next) => {
    try {
      const { product_id } = req.params;
      let data = await ProductService.findById(product_id);
      BuildResponse(req, res, {
        status_code: 200,
        data: data
      });
    } catch (err) {
      next(err);
    }
  });
  return index;
};

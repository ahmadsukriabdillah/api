const { Router } = require("express");
const index = Router();

module.exports = ({
  app: { validate, Joi },
  service: { RouteService },
  utils: { BuildResponse }
}) => {
  index.get("/get_route", async (req, res, next) => {
    try {
      var data;
      if (req.query.area_id) {
        data = await RouteService.findByArea(req.query.area_id);
      } else {
        data = await RouteService.findAll();
      }
      BuildResponse(req, res, {
        status_code: 200,
        data: data
      });
    } catch (err) {
      next(err);
    }
  });
  index.get("/itenary", async (req, res, next) => {
    try {
      const { route_id } = req.query;
      let data = await RouteService.get_itenary(route_id);
      BuildResponse(req, res, {
        status_code: 200,
        data: data
      });
    } catch (err) {
      next(err);
    }
  });
  return index;
};

const { Router } = require("express");
const index = Router();

module.exports = ({
  app: { validate, Joi },
  service: { ScheduleService },
  utils: { BuildResponse }
}) => {
  index.post(
    "/get_schedule",
    [
      validate({
        body: {
          src_id: Joi.number().required(),
          dest_id: Joi.number().required(),
          waktu_kebrangkatan: Joi.string()
            .trim()
            .max(8)
            .required()
        }
      })
    ],
    async (req, res, next) => {
      let { src_id, dest_id, waktu_kebrangkatan } = req.body;
      try {
        let data = await ScheduleService.get_schedule(
          src_id,
          dest_id,
          waktu_kebrangkatan
        );
        BuildResponse(req, res, data);
      } catch (err) {
        next(err);
      }
    }
  );
  return index;
};

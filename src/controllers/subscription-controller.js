const { Router } = require("express");
const index = Router();

module.exports = ({
  app: { validate, Joi },
  service: { SubscriptionService },
  utils: { BuildResponse }
}) => {
  index.get("/", async (req, res, next) => {
    try {
      let data = await SubscriptionService.findAll();
      BuildResponse(req, res, {
        status_code: 200,
        data: data
      });
    } catch (err) {
      next(err);
    }
  });

  index.get("/:subscription_id", async (req, res, next) => {
    try {
      const { subscription_id } = req.params;
      let data = await SubscriptionService.findById(subscription_id);
      BuildResponse(req, res, {
        status_code: 200,
        data: data
      });
    } catch (err) {
      next(err);
    }
  });
  return index;
};

const { Router } = require("express");
const index = Router();

module.exports = ({
  app: { validate, Joi },
  service: { TicketService },
  utils: { BuildResponse }
}) => {
  index.get("/", async (req, res, next) => {
    try {
      let data = await TicketService.findByUser(req.user.id);
      BuildResponse(req, res, {
        status_code: 200,
        date: new Date(),
        data: data
      });
    } catch (err) {
      next(err);
    }
  });
  index.get(
    "/:ticket_id",
    [
      validate({
        params: {
          ticket_id: Joi.number().required()
        }
      })
    ],
    async (req, res, next) => {
      try {
        const { ticket_id } = req.params;
        let data = await TicketService.findById(ticket_id);
        BuildResponse(req, res, {
          status_code: 200,
          data: data
        });
      } catch (err) {
        next(err);
      }
    }
  );
  return index;
};

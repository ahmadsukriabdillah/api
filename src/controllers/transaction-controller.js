const { Router } = require("express");
const index = Router();

module.exports = ({
  app: { validate, Joi },
  service: { TransactionService },
  utils: { BuildResponse }
}) => {
  index.get("/", async (req, res, next) => {
    try {
      let data = await TransactionService.findAll({
        user_id: req.user.id
      });
      BuildResponse(req, res, {
        status_code: 200,
        data
      });
    } catch (err) {
      next(err);
    }
  });

  index.get("/:transaction_id", async (req, res, next) => {
    try {
      let { transaction_id } = req.params;
      let data = await TransactionService.findById(transaction_id);
      BuildResponse(req, res, {
        status_code: 200,
        data
      });
    } catch (err) {
      next(err);
    }
  });
  index.post(
    "/booking",
    [
      validate({
        body: {
          product_id: Joi.number().required(),
          qty: Joi.number().required(),
          type: Joi.number().required()
        }
      })
    ],
    async (req, res, next) => {
      try {
        const { product_id, qty, type } = req.body;
        let data = await TransactionService.create_booking({
          user_id: req.user.id,
          product_id: product_id,
          qty: qty,
          type: type
        });
        BuildResponse(req, res, {
          status_code: 200,
          data
        });
      } catch (err) {
        next(err);
      }
    }
  );
  index.post(
    "/:transaction_id/use_voucher",
    [
      validate({
        params: {
          transaction_id: Joi.string().required()
        },
        body: {
          voucher_code: Joi.string().required()
        }
      })
    ],
    async (req, res, next) => {
      try {
        let data = await TransactionService.update_booking_with_voucher(
          req.params.transaction_id,
          req.body.voucher_code
        );
        BuildResponse(req, res, {
          status_code: 200,
          data
        });
      } catch (err) {
        next(err);
      }
    }
  );
  index.post(
    "/:transaction_id/payment",
    [
      validate({
        params: {
          transaction_id: Joi.string().required()
        }
      })
    ],
    async (req, res, next) => {
      try {
        const { transaction_id } = req.params;
        let data = await TransactionService.issued_payment(transaction_id);
        BuildResponse(req, res, {
          status_code: 200,
          data
        });
      } catch (err) {
        console.log(err);
        next(err);
      }
    }
  );
  index.post(
    "/:transaction_id/select_payment_method",
    [
      validate({
        params: {
          transaction_id: Joi.string().required()
        },
        body: {
          payment_type: Joi.number().required()
        }
      })
    ],
    async (req, res, next) => {
      try {
        const { transaction_id } = req.params;
        const { payment_type } = req.body;
        let data = await TransactionService.update_select_payment_method(
          transaction_id,
          payment_type
        );
        BuildResponse(req, res, {
          status_code: 200,
          data
        });
      } catch (err) {
        next(err);
      }
    }
  );
  return index;
};

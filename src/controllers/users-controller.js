const { Router } = require("express");
const index = Router();

module.exports = ({
  app: { validate, Joi },
  service: { UserService },
  utils: { BuildResponse }
}) => {
  index.get("/me", async (req, res, next) => {
    try {
      let data = await UserService.my_profile(req);
      BuildResponse(req, res, data);
    } catch (err) {
      next(err);
    }
  });
  index.post(
    "/register",
    [
      validate({
        body: {
          email: Joi.string()
            .email()
            .max(50)
            .required(),
          password: Joi.string().required(),
          first_name: Joi.string().required(),
          last_name: Joi.string().required(),
          phone_number: Joi.string().required(),
          photo_url: Joi.string().required()
        }
      })
    ],
    async (req, res, next) => {
      try {
        let data = await UserService.register_basic(req);
        BuildResponse(req, res, data);
      } catch (err) {
        next(err);
      }
    }
  );

  index.post(
    "/login_seamless",
    [
      validate({
        body: {
          uid: Joi.string().required(),
          type: Joi.string().required(),
          full_name: Joi.string().required(),
          email: Joi.string()
            .email()
            .required(),
          device_id: Joi.string().required()
        }
      })
    ],
    async (req, res, next) => {
      try {
        let data = await UserService.authentication_seamless(req.body);
        BuildResponse(req, res, {
          status_code: 200,
          data: data
        });
      } catch (err) {
        next(err);
      }
    }
  );
  index.post(
    "/login",
    [
      validate({
        body: {
          email: Joi.string()
            .email()
            .max(50),
          password: Joi.string(),
          type: Joi.string()
            .alphanum()
            .max(10)
            .required()
        }
      })
    ],
    async (req, res, next) => {
      try {
        let { email, type, uid } = req.body;
        let data;
        switch (type) {
          case "facebook":
            data = await UserService.authentication_facebook(req);
            BuildResponse(req, res, data);
            break;
          case "google":
            data = await UserService.authentication_google(req);
            BuildResponse(req, res, data);
            break;
          case "basic":
            data = await UserService.authentication_basic(req);
            BuildResponse(req, res, data);
            break;
          default:
            throw new Error("Unknow Method");
        }
      } catch (err) {
        next(err);
      }
    }
  );
  return index;
};

const { Router } = require("express");
const index = Router();

module.exports = ({
  app: { validate, Joi },
  service: { VoucherService },
  utils: { BuildResponse }
}) => {
  index.post("/check_validity", async (req, res, next) => {
    try {
      const { code } = req.body;
      let data = await VoucherService.checkVoucher(code);
      BuildResponse(req, res, data);
    } catch (err) {
      next(err);
    }
  });
  return index;
};

const WHITELIST = [
  "/",
  "/api/auth/login",
  "/api/auth/login_seamless",
  "/api/auth/register",
  "/public/**"
];
var jwt = require("express-jwt");
var path = require("path");

module.exports = (app, service, utils, controller) => {
  app.use(
    "/public/midtrans",
    controller.midtrans_controller({ app, service, utils })
  );
  app.get("/public/privacy_police", (req, res, next) => {
    res.sendFile(path.resolve("./html/privacy_police.html"));
  });
  app.use(
    jwt({
      secret: utils.config.secret,
      getToken: req => {
        if (req.headers.authorization) {
          return req.headers.authorization;
        } else if (req.query && req.query.token) {
          return req.query.token;
        }
        return null;
      }
    }).unless({ path: WHITELIST })
  );
  app.get("/", (req, res) => {
    res.status(200).json({
      status: 200,
      messsage: "server online",
      date: Date.now()
    });
  });
  app.use("/api/auth", controller.user_controller({ app, service, utils }));
  app.use("/api/area", controller.area_controller({ app, service, utils }));
  app.use("/api/route", controller.route_controller({ app, service, utils }));
  app.use(
    "/api/schedule",
    controller.schedule_controller({ app, service, utils })
  );
  app.use(
    "/api/voucher",
    controller.voucher_controller({ app, service, utils })
  );
  app.use(
    "/api/subscription",
    controller.subscription_controller({ app, service, utils })
  );
  app.use(
    "/api/transaction",
    controller.transaction_controller({ app, service, utils })
  );
  app.use(
    "/api/product",
    controller.product_controller({ app, service, utils })
  );
  app.use(
    "/api/payment",
    controller.payment_controller({ app, service, utils })
  );
  app.use("/api/ticket", controller.ticket_controller({ app, service, utils }));
};

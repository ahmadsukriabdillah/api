const { db, logger, config, FailedTaskExecution } = require("./../utils");
const findAllWithLatLing = async (latitude, longitude) => {
  try {
    let areas = await db("m_area")
      .select("id", "name")
      .where({ is_active: 1, is_deleted: 0 });
    let response = await areas.map(async area => {
      console.log([latitude || 0, longitude || 0, area.id]);
      let checkpoint = await db.raw(
        `
        select c.id, c.code, c.name, c.latitude, c.longitude, c.ref_id, c.is_shuttle, get_distance(c.latitude, c.longitude, ?, ?) as distance
        from m_checkpoint c 
        left join m_area_chekpoint_rel r on r.checkpoint_id = c.id 
        where c.is_active = 1 and is_shuttle = 1 and r.area_id = ?
        order by distance asc
      `,
        [latitude || 0, longitude || 0, area.id]
      );
      return {
        ...area,
        checkpoint: checkpoint[0]
      };
    });
    return await Promise.all(response);
  } catch (e) {
    if (!e instanceof FailedTaskExecution) {
      logger.error(__dirname + e.message);
    }
    throw new FailedTaskExecution("Failed to perfrom request", e.message);
  }
};

const findAll = async () => {
  try {
    let areas = await db("m_area")
      .select("id", "name")
      .where({ is_active: 1, is_deleted: 0 });
    let response = await areas.map(async area => {
      let checkpoint = await db.raw(
        `
        select c.id, c.code, c.name, c.latitude, c.longitude, c.ref_id, c.is_shuttle
        from m_checkpoint c 
        left join m_area_chekpoint_rel r on r.checkpoint_id = c.id 
        where c.is_active = 1 and is_shuttle = 1 and r.area_id = ?
      `,
        [area.id]
      );
      return {
        ...area,
        checkpoint: checkpoint[0]
      };
    });
    return await Promise.all(response);
  } catch (e) {
    if (!e instanceof FailedTaskExecution) {
      logger.error(__dirname + e.message);
    }
    throw new FailedTaskExecution("Failed to perfrom request", e.message);
  }
};
module.exports = {
  findAll,
  findAllWithLatLing
};

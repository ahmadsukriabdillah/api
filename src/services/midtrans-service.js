const { db, midtrans_client, encode_base64 } = require("./../utils");

const bni_bank_transfer = async transaction => {
  if (transaction.status == 200) {
    await cancel(transaction.id);
  }
  const user = await db("m_user_profile")
    .where({ user_id: transaction.user_id })
    .first();
  let parameter = {
    payment_type: "bank_transfer",
    transaction_details: {
      gross_amount: transaction.total_price,
      order_id: transaction.id
    },
    customer_details: {
      first_name: user.full_name,
      last_name: user.full_name,
      email: user.email,
      phone: user.phone_number
    },
    bank_transfer: {
      bank: "bni"
    }
  };
  const { data } = await midtrans_client.post("/charge", parameter);
  await insert_log(data);
  if (data.status_code == "201") {
    if (data.transaction_status == "pending") {
      await db("trn_transaction")
        .update({
          status: 200,
          payment_ref_id: data.transaction_id,
          method: "va",
          va_number: data.va_numbers[0].va_number,
          hyperlink: null,
          payment_data: encode_base64(data)
        })
        .where({ id: data.order_id });
    } else {
      throw new Error(data.status_message);
    }
  } else {
    throw new Error(data.status_message);
  }
};

const mandiri_bank_transfer = async transaction => {
  if (transaction.status == 200) {
    await cancel(transaction.id);
  }
  const user = await db("m_user_profile")
    .where({ user_id: transaction.user_id })
    .first();
  let parameter = {
    payment_type: "echannel",
    transaction_details: {
      gross_amount: transaction.total_price,
      order_id: transaction.id
    },
    customer_details: {
      first_name: user.full_name,
      last_name: user.full_name,
      email: user.email,
      phone: user.phone_number
    },
    echannel: {
      bill_info1: "BUSTIKET",
      bill_info2: transaction.type + " - " + transaction.product_detail.name
    }
  };
  const { data } = await midtrans_client.post("/charge", parameter);
  await insert_log(data);
  if (data.status_code == "201") {
    if (data.transaction_status == "pending") {
      await db("trn_transaction")
        .update({
          status: 200,
          payment_ref_id: data.transaction_id,
          method: "va",
          va_number: data.biller_code + data.bill_key,
          hyperlink: null,
          payment_data: encode_base64(data)
        })
        .where({ id: data.order_id });
    } else {
      throw new Error(data.status_message);
    }
  } else {
    throw new Error(data.status_message);
  }
};

const bca_bank_transfer = async transaction => {
  if (transaction.status == 200) {
    await cancel(transaction.id);
  }
  const user = await db("m_user_profile")
    .where({ user_id: transaction.user_id })
    .first();
  let parameter = {
    payment_type: "bank_transfer",
    transaction_details: {
      gross_amount: transaction.total_price,
      order_id: transaction.id
    },
    customer_details: {
      first_name: user.full_name,
      last_name: user.full_name,
      email: user.email,
      phone: user.phone_number
    },
    bank_transfer: {
      bank: "bca"
    }
  };
  const { data } = await midtrans_client.post("/charge", parameter);
  await insert_log(data);
  if (data.status_code == "201") {
    if (data.transaction_status == "pending") {
      await db("trn_transaction")
        .update({
          status: 200,
          payment_ref_id: data.transaction_id,
          method: "va",
          va_number: data.va_numbers[0].va_number,
          hyperlink: null,
          payment_data: encode_base64(data)
        })
        .where({ id: data.order_id });
    } else {
      throw new Error(data.status_message);
    }
  } else {
    throw new Error(data.status_message);
  }
};

const go_pay = async transaction => {
  if (transaction.status == 200) {
    await cancel(transaction.id);
  }
  let user = await db("m_user_profile")
    .where({ user_id: transaction.user_id })
    .first();
  let parameter = {
    payment_type: "gopay",
    transaction_details: {
      gross_amount: transaction.total_price,
      order_id: transaction.id
    },
    customer_details: {
      first_name: user.full_name || "",
      last_name: user.full_name || "",
      email: user.email || "",
      phone: user.phone_number || ""
    },
    gopay: {
      enable_callback: false,
      callback_url:
        "bustiket://check_transaction?transaction_id=" + transaction.id
    }
  };
  const { data } = await midtrans_client.post("/charge", parameter);
  await insert_log(data);
  if (data.status_code == "201") {
    if (data.transaction_status == "pending") {
      await db("trn_transaction")
        .update({
          status: 200,
          payment_ref_id: data.transaction_id,
          method: "hyperlink",
          va_number: null,
          hyperlink: data.actions.filter(x => x.name == "deeplink-redirect")[0]
            .url,
          payment_data: encode_base64(data)
        })
        .where({ id: data.order_id });
    } else {
      throw new Error(data.status_message);
    }
  } else {
    throw new Error(data.status_message);
  }
};

const permata_bank_transfer = async transaction => {
  if (transaction.status == 200) {
    await cancel(transaction.id);
  }
  let user = await db("m_user_profile")
    .where({ user_id: transaction.user_id })
    .first();
  let parameter = {
    payment_type: "bank_transfer",
    transaction_details: {
      gross_amount: transaction.total_price,
      order_id: transaction.id
    },
    customer_details: {
      first_name: user.full_name || "",
      last_name: user.full_name || "",
      email: user.email || "",
      phone: user.phone_number || ""
    },
    bank_transfer: {
      bank: "permata",
      permata: {
        recipient_name: user.full_name
      }
    }
  };
  const { data } = await midtrans_client.post("/charge", parameter);
  await insert_log(data);
  if (data.status_code == "201") {
    if (data.transaction_status == "pending") {
      await db("trn_transaction")
        .update({
          status: 200,
          payment_ref_id: data.transaction_id,
          method: "va",
          va_number: data.permata_va_number,
          hyperlink: null,
          payment_data: encode_base64(data)
        })
        .where({ id: data.order_id });
    } else {
      throw new Error(data.status_message);
    }
  } else {
    throw new Error(data.status_message);
  }
};
const cancel = async transaction_id => {
  await midtrans_client.post(`/${transaction_id}/cancel`);
};

const insert_log = async data => {
  await db("log_midtrans").insert({
    transaction_id: data.order_id,
    type: data.payment_type,
    data: encode_base64(data)
  });
};
module.exports = {
  go_pay,
  bni_bank_transfer,
  mandiri_bank_transfer,
  permata_bank_transfer,
  bca_bank_transfer
};

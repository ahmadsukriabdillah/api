const {
  db,
  logger,
  config,
  generator,
  midtrans,
  FailedTaskExecution
} = require("./../utils");
const midtrans_service = require("./midtrans-service");

const findPaymentTypeById = async type => {
  return await db("m_payment_type")
    .where({ id: type })
    .first();
};

const findAllPaymentType = async () => {
  try {
    let payment_types = await db("m_payment_type").whereRaw(
      "is_active = 1 and parent_id is null"
    );

    let data = await payment_types.map(async payment_type => {
      let childs = await db("m_payment_type").where({
        parent_id: payment_type.id
      });
      return Object.assign(payment_type, {
        childs: childs
      });
    });
    return Promise.all(data);
  } catch (e) {
    if (!e instanceof FailedTaskExecution) {
      logger.error(__dirname + e.message);
    }
    throw new FailedTaskExecution("Failed to perfrom request", e.message);
  }
};

const charge = async transaction => {
  switch (transaction.payment_type) {
    case 2:
      await midtrans_service.go_pay(transaction, transaction.payment_type);
      break;
    case 5:
      await midtrans_service.bni_bank_transfer(transaction);
      break;
    case 6:
      await midtrans_service.bca_bank_transfer(transaction);
      break;
    case 7:
      await midtrans_service.mandiri_bank_transfer(transaction);
      break;
    case 8:
      await midtrans_service.permata_bank_transfer(transaction);
      break;
    default:
      throw new FailedTaskExecution("Methode Payment tidak di ketahui");
  }
};
module.exports = {
  findPaymentTypeById,
  findAllPaymentType,
  charge
};

const { db, logger, config, FailedTaskExecution } = require("./../utils");

const findAll = async () => {
  try {
    let products = await db("m_product").where({ is_active: 1, is_deleted: 0 });

    let promise = [];
    products.forEach(product => {
      promise.push(findById(product.id));
    });
    let response = await Promise.all(promise);
    return response;
  } catch (e) {
    if (!e instanceof FailedTaskExecution) {
      logger.error(__dirname + e.message);
    }
    throw new FailedTaskExecution("Failed to perfrom request", e.message);
  }
};
const findById = async (product_id, quota = 0) => {
  try {
    let product = await db("m_product")
      .select("id", "name", "description", "price")
      .where({ id: product_id })
      .first();
    if (quota != 0) {
      product.quota = quota;
    }
    return {
      ...product,
      valid_for_route: await findValidRouteProduct(product.id)
    };
  } catch (e) {
    if (!e instanceof FailedTaskExecution) {
      logger.error(__dirname + e.message);
    }
    throw new FailedTaskExecution("Failed to perfrom request", e.message);
  }
};

const findValidRouteProduct = async product_id => {
  try {
    let route = await db.raw(
      `
        select CONCAT(src.code,'-',dest.code) as code,src.code as src_code, src.name as src_name , dest.code as dest_code, dest.name as dest_name 
        from m_route r
        left join m_checkpoint src on r.source_id = src.id
        left join m_checkpoint dest on r.destination_id = dest.id
        where r.product_id = ?    
    `,
      product_id
    );
    return route[0];
  } catch (e) {
    if (!e instanceof FailedTaskExecution) {
      logger.error(__dirname + e.message);
    }
    throw new FailedTaskExecution("Failed to perfrom request", e.message);
  }
};
module.exports = {
  findAll,
  findById
};

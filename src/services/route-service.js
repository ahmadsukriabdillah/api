const { db, logger, config, FailedTaskExecution } = require("./../utils");

const findAll = async () => {
  try {
    let checkpoints = await db("m_checkpoint")
      .select("id", "code", "name", "latitude", "longitude", "ref_id")
      .where({ is_active: 1, is_deleted: 0, is_shuttle: 1 });

    return checkpoints;
  } catch (e) {
    if (!e instanceof FailedTaskExecution) {
      logger.error(__dirname + e.message);
    }
    throw new FailedTaskExecution("Failed to perfrom request", e.message);
  }
};

const findByArea = async area_id => {
  try {
    let checkpoint = await db.raw(
      `
      select c.id, c.code, c.name, c.latitude, c.longitude, c.ref_id, c.is_shuttle 
      from m_checkpoint c 
      left join m_area_chekpoint_rel r on r.checkpoint_id = c.id 
      where c.is_active = 1 and is_shuttle = 1 and r.area_id = ? 
    `,
      [area_id]
    );

    return checkpoint[0];
  } catch (e) {
    if (!e instanceof FailedTaskExecution) {
      logger.error(__dirname + e.message);
    }
    throw new FailedTaskExecution("Failed to perfrom request", e.message);
  }
};

const get_itenary = async route_id => {
  try {
    let itenary = await db.raw(
      `
      SELECT i.squance, c.name , c.latitude, c.longitude, c.is_shuttle from m_route_itenary i 
      left join m_checkpoint c on i.checkpoint_id = c.id
      where i.route_id = ? and c.is_active = 1
      order by i.squance asc
      `,
      [route_id]
    );

    return itenary[0];
  } catch (e) {
    if (!e instanceof FailedTaskExecution) {
      logger.error(__dirname + e.message);
    }
    throw new FailedTaskExecution("Failed to perfrom request", e.message);
  }
};
module.exports = {
  findAll,
  get_itenary,
  findByArea
};

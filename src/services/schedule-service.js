const { db, logger, config, FailedTaskExecution } = require("./../utils");

const get_schedule = async (src_id, dest_id, waktu_kebrangkatan) => {
  try {
    let exist = await db
      .select("id", "source_id", "destination_id")
      .from("m_route")
      .where({
        is_active: 1,
        is_deleted: 0,
        source_id: src_id,
        destination_id: dest_id
      })
      .first();
    if (exist) {
      let schedule = await db.raw(
        `
          select s.route_id ,src.code as src_code, src.name as src_name, dest.code as dest_code, dest.name as dest_name ,
            get_distance(src.latitude, src.longitude, dest.latitude, dest.longitude) as jarak, 
            s.estimate_speed,get_distance_eta(src.latitude, src.longitude, dest.latitude, dest.longitude, s.estimate_speed) as eta ,schedule ,
            v.code as bus_code,p.price, p.id as product_id
          from m_route_schedule s
          left join m_route r on s.route_id = r.id
          left join m_product p on p.id = r.product_id
          left join m_vehicle v on s.vehicle_id = v.id
          left join m_checkpoint src on src.id = r.source_id
          left join m_checkpoint dest on dest.id = r.destination_id
          where s.schedule > ? and s.route_id = ?
          order by s.schedule asc
        `,
        [waktu_kebrangkatan, exist.id]
      );
      return {
        status_code: 200,
        data: schedule[0]
      };
    } else {
      return {
        status_code: 404,
        message: "Jadwal tidak ditemukan",
        developer_message: "route not found"
      };
    }
  } catch (e) {
    if (!e instanceof FailedTaskExecution) {
      logger.error(__dirname + e.message);
    }
    throw new FailedTaskExecution("Terjadi kesalahan pada sistem", e.message);
  }
};
module.exports = {
  get_schedule
};

const { db, logger, config, FailedTaskExecution } = require("./../utils");

const product_service = require("./product-service");

const findSubscriptionDetail = async subscription => {
  try {
    const { id } = subscription;
    let data = await db
      .from("m_subscription_detail")
      .where({ subscription_id: id });

    let promisees = [];
    data.forEach(product => {
      promisees.push(
        product_service.findById(product.product_id, product.quota)
      );
    });

    let response = await Promise.all(promisees);
    return {
      ...subscription,
      product_details: response
    };
  } catch (e) {
    if (!e instanceof FailedTaskExecution) {
      logger.error(__dirname + e.message);
    }
    throw new FailedTaskExecution("Failed to perfrom request", e.message);
  }
};

const findAll = async () => {
  try {
    let data = await db("m_subscription")
      .select("id", "code", "name", "description", "validity_days", "price")
      .where({ is_active: 1, is_deleted: 0 });

    let promisees = [];
    data.forEach(subs => {
      promisees.push(findSubscriptionDetail(subs));
    });
    let response = await Promise.all(promisees);

    return response;
  } catch (e) {
    if (!e instanceof FailedTaskExecution) {
      logger.error(__dirname + e.message);
    }
    throw new FailedTaskExecution("Failed to perfrom request", e.message);
  }
};

const findById = async subscription_id => {
  try {
    let data = await db("m_subscription")
      .select("id", "code", "name", "description", "validity_days", "price")
      .where({ id: subscription_id })
      .first();
    return await findSubscriptionDetail(data);
  } catch (e) {
    if (!e instanceof FailedTaskExecution) {
      logger.error(__dirname + e.message);
    }
    throw new FailedTaskExecution("Failed to perfrom request", e.message);
  }
};

module.exports = {
  findAll,
  findById
};

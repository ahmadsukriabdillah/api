const {
  db,
  knex_trx,
  logger,
  config,
  generator,
  FailedTaskExecution
} = require("./../utils");
const moment = require("moment");
const product_service = require("./../services/product-service");
const create_ticket = async (
  user_id,
  transaction_id,
  product_id,
  valid_days = 1,
  qty = 1
) => {
  let promises = [];
  for (let i = 0; i < qty; i++) {
    let data = db("trn_ticket").insert({
      user_id: user_id,
      product_id: product_id,
      ticket_code: generator.generate_booking_code(),
      transaction_id: transaction_id,
      use_before:
        valid_days == 1
          ? moment().format("YYYY-MM-D") + " " + "23:59:59"
          : moment()
              .add(valid_days, "days")
              .format("YYYY-MM-D") +
            " " +
            "23:59:59",
      status: 200
    });
    promises.push(data);
  }
  return Promise.all(promises);
};

const create_ticket_from_transaction = async transaction => {
  try {
    let promises = [];
    switch (transaction.type) {
      case 1:
        transaction.transaction_details.forEach(data => {
          let queue = create_ticket(
            transaction.user_id,
            transaction.id,
            transaction.product_id,
            1,
            data.qty
          );
          promises.push(queue);
        });
        break;
      case 2:
        transaction.product_detail.product_details.forEach(data => {
          transaction.transaction_details.forEach(x => {
            for (let i = 0; i < x.qty; i++) {
              let queue = create_ticket(
                transaction.user_id,
                transaction.id,
                data.id,
                transaction.product_detail.validity_days,
                data.quota
              );
              promises.push(queue);
            }
          });
        });
        break;
    }
    await Promise.all(promises);
  } catch (e) {
    if (!e instanceof FailedTaskExecution) {
      logger.error(__dirname + e.message);
    }
    throw new FailedTaskExecution("Failed To Issued Ticket", e.message);
  }
};

const findByUser = async user_id => {
  try {
    let products = await product_service.findAll();
    let entity = products.reduce((entities, product) => {
      return {
        ...entities,
        [product.id]: product
      };
    }, {});
    let data = await db.raw(
      `select t.*, st.opt_desc as status_string from trn_ticket t left join m_opt st on (t.status = st.opt_id and st.opt_type = 'opt_ticket_status') where user_id = ?`,
      [user_id]
    );
    let response = data[0].map(d => {
      return {
        ...d,
        product_detail: entity[d.product_id]
      };
    });
    return response;
  } catch (e) {
    if (!e instanceof FailedTaskExecution) {
      logger.error(__dirname + e.message);
    }
    throw new FailedTaskExecution("Failed To Full fill request", e.message);
  }
};

const findById = async ticket_id => {
  try {
    let data = await db.raw(
      `select t.*, st.opt_desc as status_string from trn_ticket t left join m_opt st on (t.status = st.opt_id and st.opt_type = 'opt_ticket_status') where t.ticket_id = ? limit 1`,
      [ticket_id]
    );
    let product = await product_service.findById(data[0][0].product_id);
    return {
      ...data[0][0],
      product_detail: product
    };
  } catch (e) {
    if (!e instanceof FailedTaskExecution) {
      logger.error(__dirname + e.message);
    }
    throw new FailedTaskExecution("Failed To Full fill request", e.message);
  }
};

module.exports = {
  findByUser,
  findById,
  create_ticket_from_transaction
};

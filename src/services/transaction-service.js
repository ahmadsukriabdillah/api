const {
  db,
  logger,
  config,
  knex_trx,
  FailedTaskExecution
} = require("./../utils");
const { v4 } = require("uuid");
const moment = require("moment");
const product_service = require("./product-service");
const payment_service = require("./payment-service");
const ticket_service = require("./ticket-service");

const subscription_service = require("./subscription-service");

const findDetailTransaction = async transaction => {
  try {
    let data = await db("trn_transaction_detail")
      .where({ transaction_id: transaction.id })
      .orderBy("id", "asc");
    switch (transaction.type) {
      case 1:
        transaction.product_detail = await product_service.findById(
          transaction.product_id
        );
        break;
      case 2:
        transaction.product_detail = await subscription_service.findById(
          transaction.subscription_id
        );
        break;
    }
    if (transaction.payment_type) {
      transaction.payment_type_detail = await payment_service.findPaymentTypeById(
        transaction.payment_type
      );
    } else {
      transaction.payment_type_detail = null;
    }

    return {
      ...transaction,
      transaction_details: data
    };
  } catch (e) {
    if (!e instanceof FailedTaskExecution) {
      logger.error(__dirname + e.message);
    }
    throw new FailedTaskExecution("Failed to perfrom request", e.message);
  }
};

const findAll = async ({ user_id }) => {
  try {
    let data = await db("trn_transaction as t")
      .select(
        "t.*",
        "os.opt_desc as product_type",
        "st.opt_desc as transaction_status"
      )
      .joinRaw(
        "left join m_opt as os on t.type = os.opt_id and os.opt_type = 'opt_transaction_type'"
      )
      .joinRaw(
        "left join m_opt as st on t.status = st.opt_id and st.opt_type = 'opt_transaction_status'"
      )
      .where({ "t.user_id": user_id });
    let list_detail_promise = [];
    data.forEach(transaction => {
      list_detail_promise.push(findDetailTransaction(transaction));
    });
    let response = await Promise.all(list_detail_promise);
    return response;
  } catch (e) {
    if (!e instanceof FailedTaskExecution) {
      logger.error(__dirname + e.message);
    }
    throw new FailedTaskExecution("Failed to perfrom request", e.message);
  }
};

const findById = async transaction_id => {
  try {
    let transaction = await db("trn_transaction as t")
      .select(
        "t.*",
        "os.opt_desc as product_type",
        "st.opt_desc as transaction_status"
      )
      .joinRaw(
        "left join m_opt as os on t.type = os.opt_id and os.opt_type = 'opt_transaction_type'"
      )
      .joinRaw(
        "left join m_opt as st on t.status = st.opt_id and st.opt_type = 'opt_transaction_status'"
      )
      .where({ "t.id": transaction_id })
      .first();
    switch (transaction.type) {
      case 1:
        transaction.product_detail = await product_service.findById(
          transaction.product_id
        );
        break;
      case 2:
        transaction.product_detail = await subscription_service.findById(
          transaction.subscription_id
        );
        break;
      default:
        throw new FailedTaskExecution("Unknow type transaction");
    }
    if (transaction.payment_type) {
      transaction.payment_type_detail = await payment_service.findPaymentTypeById(
        transaction.payment_type
      );
    } else {
      transaction.payment_type_detail = null;
    }
    let reponse = await findDetailTransaction(transaction);
    return reponse;
  } catch (e) {
    console.log(e);
    if (!e instanceof FailedTaskExecution) {
      logger.error(__dirname + e.message);
    }
    throw new FailedTaskExecution("Failed to perfrom request", e.message);
  }
};

const update_select_payment_method = async (transaction, payment_type) => {
  try {
    let admin = await db("m_payment_type")
      .where({ id: payment_type })
      .first();
    await db.raw(
      "update trn_transaction_detail set price = ?, sub_total = ? where transaction_id = ? and name = ?",
      [admin.admin, admin.admin, transaction, "Biaya Admin"]
    );
    await db.raw(
      "update trn_transaction set admin = ?, payment_type = ?, total_price = (select sum(sub_total) from trn_transaction_detail where transaction_id = ?) where id = ?",
      [admin.admin, payment_type, transaction, transaction]
    );
    return findById(transaction);
  } catch (e) {
    console.log(e);
    if (!e instanceof FailedTaskExecution) {
      logger.error(__dirname + e.message);
    }
    throw new FailedTaskExecution("Failed to full fill request", e.message);
  }
};

const issued_payment = async transaction_id => {
  try {
    let transaction = await findById(transaction_id);
    if (transaction.status != 100 || transaction.status != 200) {
      await payment_service.charge(transaction);
      return findById(transaction_id);
    } else {
      throw new FailedTaskExecution("Status transaksi sudah tidak aktif");
    }
  } catch (e) {
    if (!e instanceof FailedTaskExecution) {
      logger.error(__dirname + e.message);
    }
    throw new FailedTaskExecution("Failed to perfrom request", e.message);
  }
};

const update_booking_with_voucher = async (transaction_id, voucher_code) => {
  var trx = null;
  try {
    let transaction = await db("trn_transaction")
      .where({ id: transaction_id })
      .first();

    let voucher = await db("m_voucher")
      .select("*")
      .where({ code: voucher_code })
      .andWhereRaw("expired_at > ? and (quota > 0 or quota = -1)", [
        moment().format("YYYY-MM-D")
      ])
      .first();
    if (!voucher) {
      throw new FailedTaskExecution("Voucher tidak valid");
    }
    if (!transaction) {
      throw new FailedTaskExecution("Transaction tidak di temukan");
    }
    if (transaction.expired_at) trx = await knex_trx();
    if (voucher.quota != -1) {
      await trx.raw("update m_voucher set quota = quota - 1 where id = ?", [
        voucher.id
      ]);
    }
    let discount = 0.0;
    switch (voucher.method) {
      case "percentage":
        discount = transaction.total_price * (voucher.value / 100);
        break;
      case "fixed":
        discount = voucher.value;
        break;
      default:
        throw new FailedTaskExecution("failed to apply coupone code");
    }
    await trx.raw(
      "update trn_transaction_detail set price = ?, sub_total = ? where transaction_id = ? and name = ?",
      [discount * -1, discount * -1, transaction.id, "Discount"]
    );
    await trx.raw(
      "update trn_transaction set discount = ?, voucher_id = ?, total_price = (select sum(sub_total) from trn_transaction_detail where transaction_id = ?) - ? where id = ?",
      [discount, voucher.id, transaction.id, discount, transaction.id]
    );
    await trx.commit();
    let data = await findById(transaction.id);
    return data;
  } catch (e) {
    logger.error(__dirname + e.message);
    if (trx) {
      await trx.rollback();
    }
    throw new FailedTaskExecution("Failed to perfrom request", e.message);
  }
};
const create_booking = async ({ user_id, product_id, type, qty }) => {
  const trx = await knex_trx();
  try {
    let transaction_id = v4();
    let transaction = null;
    switch (type) {
      case 1:
        let product = await product_service.findById(product_id);
        transaction = await trx("trn_transaction").insert({
          id: transaction_id,
          user_id: user_id,
          type: type,
          product_id: product_id,
          total_price: qty * product.price,
          expired_at: moment()
            .add(1, "hours")
            .format("YYYY-MM-D HH:mm:ss"),
          status: 100
        });
        await trx("trn_transaction_detail").insert([
          {
            transaction_id: transaction_id,
            name: product.name,
            qty: qty,
            price: product.price,
            sub_total: qty * product.price
          },
          {
            transaction_id: transaction_id,
            name: "Biaya Admin",
            qty: 1,
            price: 0,
            sub_total: 0
          },
          {
            transaction_id: transaction_id,
            name: "Discount",
            qty: 1,
            price: 0,
            sub_total: 0
          }
        ]);
        await trx.commit();
        let data = await findById(transaction_id);
        return data;
      case 2:
        let subscription = await subscription_service.findById(product_id);
        transaction = await trx("trn_transaction")
          .insert({
            id: transaction_id,
            user_id: user_id,
            type: type,
            subscription_id: subscription.id,
            total_price: qty * subscription.price,
            expired_at: moment()
              .add(1, "hours")
              .format("YYYY-MM-D HH:mm:ss"),
            status: 100
          })
          .return("id");
        await trx("trn_transaction_detail").insert([
          {
            transaction_id: transaction_id,
            name: subscription.name,
            qty: qty,
            price: subscription.price,
            sub_total: qty * subscription.price
          },
          {
            transaction_id: transaction_id,
            name: "Biaya Admin",
            qty: 1,
            price: 0,
            sub_total: 0
          },
          {
            transaction_id: transaction_id,
            name: "Discount",
            qty: 1,
            price: 0,
            sub_total: 0
          }
        ]);
        await trx.commit();
        return await findById(transaction_id);
      default:
        throw new FailedTaskExecution("Type tidak di Kenali");
    }
  } catch (e) {
    if (!e instanceof FailedTaskExecution) {
      logger.error(__dirname + e.message);
    }
    await trx.rollback();
    throw new FailedTaskExecution("Failed to perfrom request", e.message);
  }
};
const success_payment = async ({ order_id }) => {
  try {
    let transaction = await findById(order_id);
    await ticket_service.create_ticket_from_transaction(transaction);
    await db("trn_transaction")
      .update({
        status: 300,
        success_at: moment().format("YYYY-MM-D HH:mm:ss")
      })
      .where({ id: order_id });
  } catch (e) {
    if (!e instanceof FailedTaskExecution) {
      logger.error(__dirname + e.message);
    }
  }
};
module.exports = {
  findAll,
  findById,
  create_booking,
  update_booking_with_voucher,
  issued_payment,
  update_select_payment_method,
  success_payment
};

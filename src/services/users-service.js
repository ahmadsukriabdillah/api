const {
  db,
  logger,
  config,
  UnauthorizerException,
  FailedTaskExecution
} = require("./../utils");
const moment = require("moment");
const GOOGLE_CLIENT_ID = process.env.GOOGLE_CLIENT_ID;
const client_id = process.env.FACEBOOK_CLIENT_ID;
const client_secret = process.env.FACEBOOK_CLIENT_SECRET;

const { OAuth2Client } = require("google-auth-library");
var client = new OAuth2Client(GOOGLE_CLIENT_ID, "", "");

const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const axios = require("axios");

const authentication_google = async ({ body: { uid } }) => {
  try {
    let user = await client.verifyIdToken({
      idToken: uid,
      audience: GOOGLE_CLIENT_ID
    });

    let exist = await db
      .from("m_user")
      .select("id", "uid", "type", "create_at")
      .where({
        uid: user.payload.sub,
        type: "google"
      })
      .first();
    if (exist) {
      let profile_id = await db
        .from("m_user_profile")
        .select("full_name", "last_name", "phone_number", "profil_pic")
        .where({ user_id: exist.id })
        .first();
      let token = jwt.sign(
        { id: exist.id, auth_by: exist.type },
        config.secret,
        {
          expiresIn: "1 days"
        }
      );
      return {
        status_code: 200,
        data: {
          ...exist,
          profile: profile_id,
          access_token: token
        }
      };
    } else {
      let user_id = await db("m_user").insert({
        type: "google",
        uid: user.payload.sub,
        email: user.payload.email
      });
      let profile_id = await db("m_user_profile").insert({
        user_id: user_id,
        full_name: user.payload.given_name,
        last_name: user.payload.family_name,
        profil_pic: user.payload.picture
      });
      let user_id_get = await db
        .from("m_user")
        .select("id", "uid", "type", "create_at")
        .where({
          id: user_id[0]
        })
        .first();
      let profile_id_get = await db
        .from("m_user_profile")
        .select("full_name", "last_name", "phone_number", "profil_pic")
        .where({ user_id: user_id[0] })
        .first();
      let token = jwt.sign(
        { id: user_id_get.id, auth_by: user_id_get.type },
        config.secret,
        {
          expiresIn: "1 days"
        }
      );
      return {
        status_code: 200,
        data: {
          ...user_id_get,
          profile: profile_id_get,
          access_token: token
        }
      };
    }
  } catch (err) {
    throw new UnauthorizerException(
      "Failed to login by google.",
      "Token google tidak valid"
    );
  }
};

const authentication_facebook = async ({ body: { uid } }) => {
  try {
    var get_access_token = await axios.get(
      "https://graph.facebook.com/oauth/access_token",
      {
        params: {
          client_id: client_id,
          client_secret: client_secret,
          grant_type: "client_credentials"
        }
      }
    );

    var get_user_id = await axios.get(
      "https://graph.facebook.com/debug_token",
      {
        params: {
          input_token: uid,
          access_token: get_access_token.data.access_token
        }
      }
    );

    let exist = await db
      .from("m_user")
      .select("id", "uid", "type", "create_at")
      .where({
        uid: get_user_id.data.data.user_id,
        type: "facebook"
      })
      .first();
    if (exist) {
      let profile_id = await db
        .from("m_user_profile")
        .select("full_name", "last_name", "phone_number", "profil_pic")
        .where({ user_id: exist.id })
        .first();
      let token = jwt.sign(
        { id: exist.id, auth_by: exist.type },
        config.secret,
        {
          expiresIn: "1 days"
        }
      );
      return {
        status_code: 200,
        data: {
          ...exist,
          profile: profile_id,
          access_token: token
        }
      };
    } else {
      var get_user_info = await axios.get(
        `https://graph.facebook.com/${get_user_id.data.data.user_id}`,
        {
          params: {
            fields:
              "id,name,full_name,middle_name,last_name,picture,short_name,email",
            access_token: get_access_token.data.access_token
          }
        }
      );
      console.log(get_user_info);
      let {
        id,
        name,
        full_name,
        middle_name,
        last_name,
        picture,
        short_name,
        email
      } = get_user_info.data;
      let user_id = await db("m_user").insert({
        type: "facebook",
        uid: get_user_id.data.data.user_id,
        email: email || ""
      });
      let profile_id = await db("m_user_profile").insert({
        user_id: user_id,
        full_name: full_name,
        last_name: last_name,
        profil_pic: picture.data.url
      });

      let user_id_get = await db
        .from("m_user")
        .select("id", "uid", "type", "create_at")
        .where({
          id: user_id[0]
        })
        .first();
      let profile_id_get = await db
        .from("m_user_profile")
        .select("full_name", "last_name", "phone_number", "profil_pic")
        .where({ user_id: user_id[0] })
        .first();
      let token = jwt.sign(
        { id: user_id_get.id, auth_by: user_id_get.type },
        config.secret,
        {
          expiresIn: "1 days"
        }
      );
      return {
        status_code: 200,
        data: {
          ...user_id_get,
          profile: profile_id_get,
          access_token: token
        }
      };
    }
  } catch (e) {
    throw new UnauthorizerException(
      "Failed to login by facebook.",
      "Token facebook tidak valid"
    );
  }
};

const register_basic = async ({
  body: { email, password, full_name, last_name, phone_number }
}) => {
  let exist = await db("m_user")
    .where({
      email: email
    })
    .first();

  if (exist) {
    throw new FailedTaskExecution(
      `User with email address '${email}' is exist. please login`
    );
  }
  let password_hash = await bcrypt.hash(password, 10);

  const res = await db.transaction(async trx => {
    let user_id = await trx("m_user").insert({
      type: "basic",
      email: email,
      password: password_hash
    });
    let profile_id = await trx("m_user_profile").insert({
      user_id: user_id,
      full_name: full_name,
      last_name: last_name,
      phone_number: phone_number
    });

    return [user_id[0], profile_id[0]];
  });
  return {
    status_code: 200,
    message: "Succesfully Register",
    developer_message: "Succesfully Register",
    res: res
  };
};

const authentication_basic = async ({ body: { email, password } }) => {
  let user = await db
    .from("m_user")
    .select("id", "uid", "type", "create_at", "password")
    .where({ email: email, type: "basic" })
    .first();

  console.log(user);
  if (!user) {
    throw new UnauthorizerException(
      "Invalid Username / Password.",
      "Username / password tidak benar. user tidak di temukan"
    );
  } else {
    const match = await bcrypt.compare(password, user.password || "");
    if (match) {
      let profile_id_get = await db
        .from("m_user_profile")
        .select("full_name", "last_name", "phone_number", "profil_pic")
        .where({ user_id: user.id })
        .first();
      delete user.password;
      let token = jwt.sign({ id: user.id, auth_by: user.type }, config.secret, {
        expiresIn: "1 days"
      });
      return {
        status_code: 200,
        data: {
          ...user,
          profile: profile_id_get,
          access_token: token
        }
      };
    } else {
      throw new UnauthorizerException(
        "Invalid Username / Password.",
        "Username / password tidak benar."
      );
    }
  }
};

const authentication_seamless = async ({
  uid,
  type,
  full_name,
  email,
  photo_url,
  device_id
}) => {
  let user = await db
    .from("m_user")
    .select("id", "uid", "email", "type", "create_at")
    .where({ uid: uid, type: type })
    .first();
  if (user) {
    let profile_id_get = await db
      .from("m_user_profile")
      .select("full_name", "phone_number", "profil_pic")
      .where({ user_id: user.id })
      .first();
    await db("m_user")
      .update({
        device_id: device_id,
        last_login: moment().format("YYYY-MM-D hh:mm:s")
      })
      .where({ id: user.id });
    let token = jwt.sign({ id: user.id, auth_by: user.type }, config.secret, {
      expiresIn: "1 days"
    });
    return {
      ...user,
      profile: profile_id_get,
      access_token: token
    };
  } else {
    let user_id = await db("m_user").insert({
      type: type,
      uid: uid,
      email: email,
      device_id: device_id,
      last_login: moment().format("YYYY-MM-D hh:mm:s")
    });
    let profile_id = await db("m_user_profile").insert({
      user_id: user_id,
      full_name: full_name,
      profil_pic: photo_url
    });
    let user_id_get = await db
      .from("m_user")
      .select("id", "uid", "email", "type", "create_at")
      .where({
        id: user_id[0]
      })
      .first();
    let profile_id_get = await db
      .from("m_user_profile")
      .select("full_name", "phone_number", "profil_pic")
      .where({ user_id: user_id[0] })
      .first();
    let token = jwt.sign(
      { id: user_id_get.id, auth_by: user_id_get.type },
      config.secret,
      {
        expiresIn: "1 days"
      }
    );
    return {
      ...user_id_get,
      profile: profile_id_get,
      access_token: token
    };
  }
};

const my_profile = async req => {
  let user = await db
    .from("m_user")
    .select("id", "uid", "email", "type", "create_at")
    .where({ id: req.user.id })
    .first();
  let profile_id_get = await db
    .from("m_user_profile")
    .select("full_name", "phone_number", "profil_pic")
    .where({ user_id: req.user.id })
    .first();
  return {
    status_code: 200,
    data: {
      ...user,
      profile: profile_id_get
    }
  };
};

module.exports = {
  authentication_basic,
  authentication_google,
  authentication_facebook,
  register_basic,
  my_profile,
  authentication_seamless
};

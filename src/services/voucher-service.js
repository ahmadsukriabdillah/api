const { db, logger, config, FailedTaskExecution } = require("./../utils");
const moment = require("moment");

const checkValidityVoucher = async code => {
  try {
    let voucher = await db("m_voucher")
      .select("*")
      .where({ code: code })
      .andWhereRaw("expired_at > ?", [moment().format("YYYY-MM-D")])
      .first();
    if (voucher) {
      return voucher;
    } else {
      throw new FailedTaskExecution("Voucher tidak valid");
    }
  } catch (e) {
    if (!e instanceof FailedTaskExecution) {
      logger.error(__dirname + e.message);
    }
    throw new FailedTaskExecution("Failed to perfrom request");
  }
};

const checkVoucher = async code => {
  try {
    let voucher = await db("m_voucher")
      .select("*")
      .where({ code: code })
      .andWhereRaw("expired_at > ?", [moment().format("YYYY-MM-D")])
      .first();
    if (voucher) {
      return {
        status_code: 200,
        data: voucher
      };
    } else {
      return {
        status_code: 404,
        message: "Voucher tidak di valid"
      };
    }
  } catch (e) {
    if (!e instanceof FailedTaskExecution) {
      logger.error(__dirname + e.message);
    }
    throw new FailedTaskExecution("Failed to perfrom request", e.message);
  }
};
module.exports = {
  checkVoucher,
  checkValidityVoucher
};

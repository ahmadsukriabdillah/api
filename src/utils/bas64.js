const encode_base64 = payload => {
  if (payload instanceof Object) {
    payload = JSON.stringify(payload);
  }
  let buff = new Buffer(payload);
  return buff.toString("base64");
};

const decode_base64 = payload => {
  return Buffer.from(payload, "base64").toString("ascii");
};

module.exports = {
  encode_base64,
  decode_base64
};

let Response = () => {
  return {
    status_code: 200,
    message: "Succesfull",
    developer_message: "Succesfull"
  };
};
const BuildResponse = (req, res, payload) => {
  res
    .status(payload.status_code || 200)
    .json(
      Object.assign(Response(), {
        ...payload
      })
    )
    .end();
};

module.exports = {
  Response,
  BuildResponse
};

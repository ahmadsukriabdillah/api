const ev = require("express-validation");
const { Response } = require("./base.response");
const { ExceptionMapper } = require("./exception");
const error_handler = (err, res) => {
  if (err instanceof ev.ValidationError) {
    return res
      .status(err.status || 400)
      .json(
        Object.assign(Response(), {
          status_code: err.status,
          message: "Missing required parameter",
          developer_message: err.message,
          validation_error: err.errors.map(data => {
            return {
              field: data.field,
              location: data.location,
              message: data.messages[0]
            };
          })
        })
      )
      .end();
  }

  if (err.code === "invalid_token" && err.name === "UnauthorizedError") {
    return res
      .status(401)
      .json({
        status_code: 401,
        message: "Unauthorization request.",
        developer_message: "Token tidak di kenali"
      })
      .end();
  }
  if (err.code === "invalid_token" && err.name === "TokenExpiredError") {
    return res
      .status(401)
      .json({
        status_code: 401,
        message: "Unauthorization Request.",
        developer_message: "Token kadaluarsa login kembali"
      })
      .end();
  }
  if (err.code === "credentials_required") {
    return res
      .status(401)
      .json({
        status_code: 401,
        message: "Unauthorization Request.",
        developer_message: "Missing authorization token"
      })
      .end();
  }
  let response = ExceptionMapper.filter(element => {
    return err instanceof element;
  });
  if (response[0]) {
    return res
      .status(err.status_code)
      .json(err.toJson())
      .end();
  } else {
    console.log(err.message);
    return res
      .status(500)
      .send({
        status_code: 500,
        message: "Somthing went wrong.",
        developer_message: err.stack
      })
      .end();
  }
};

module.exports = {
  error_handler
};

const getCharacter = possible => {
  return possible.charAt(Math.floor(Math.random() * possible.length));
};

const generate_code = (length, every = [], prefix = "") => {
  let delimiter = "-";
  let possible = "ABCDEFGHJKMNPQRSTUXYZ1234567890";

  for (var i = prefix.length; i < length; i++) {
    let character = getCharacter(possible);
    if (every.length > 0) {
      if (every[0] == i) {
        prefix += delimiter;
        every.splice(0, 1);
      }
    }
    while (prefix.length > 0 && character === prefix.substr(-1)) {
      character = getCharacter(possible);
    }
    prefix += character;
  }
  return prefix;
};

const generate_booking_code = () => {
  return generate_code(12, [3, 6]);
};

const generate_payment_code = () => {
  return generate_code(12, [6], "PAY-");
};
let generator = exports;
generator.generate_booking_code = generate_booking_code;
generator.generate_payment_code = generate_payment_code;

const db = require("./db");
const logger = require("./logger");
const Pagination = require("./pagging");
const error_handler = require("./error-handler");
const config = require("./config");
const generator = require("./generator");
const midtrans = require("./midtrans");
const base_response = require("./base.response");
const exception = require("./exception");
const base64 = require("./bas64");
module.exports = {
  ...midtrans,
  generator,
  config,
  ...error_handler,
  ...Pagination,
  ...logger,
  ...db,
  ...base_response,
  ...exception,
  ...base64
};

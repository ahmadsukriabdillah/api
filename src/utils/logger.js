const isProduction = process.env.NODE_ENV !== "production";

const { createLogger, format, transports } = require("winston");
const { combine, timestamp, printf, simple, colorize, align } = format;

const getDate = () => {
  let d = new Date();
  (month = "" + (d.getMonth() + 1)),
    (day = "" + d.getDate()),
    (year = d.getFullYear());

  if (month.length < 2) month = "0" + month;
  if (day.length < 2) day = "0" + day;

  return [year, month, day].join("-");
};
const logger = createLogger({
  format: format.combine(format.splat(), format.simple()),
  transports: [
    new transports.File({
      filename: `logs/${getDate()}-combined.log`,
      level: "info"
    }),
    new transports.File({
      filename: `logs/${getDate()}-errors.log`,
      level: "error"
    }),
    new transports.Console({
      format: combine(
        colorize(),
        timestamp(),
        align(),
        printf(info => `${info.timestamp} ${info.level} ${info.message}`)
      )
    })
  ],
  exitOnError: false // do not exit on handled exceptions
});

logger.stream = {
  write: function(message, encoding) {
    logger.info(message);
  }
};

module.exports = {
  logger
};

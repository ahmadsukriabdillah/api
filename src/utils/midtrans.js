const axios = require("axios");

const midtrans_client = axios.create({
  baseURL: process.env.MIDTRANS_BASE_URL,
  auth: {
    username: process.env.MIDTRANS_SERVER_KEY,
    password: ""
  }
});

module.exports = {
  midtrans_client
};

const Pagination = async (
  sql,
  sql_count,
  { limit = 100, page_number = 1, order_by, sort_by, pagination = "true" }
) => {
  const [result, count] = await Promise.all([
    new Promise((resolve, reject) => {
      if (pagination === "true") {
        sql
          .offset(parseInt(limit) * parseInt(page_number) - parseInt(limit))
          .limit(parseInt(limit))
          .orderBy(sort_by, order_by)
          .then(result => {
            resolve(result);
          })
          .catch(err => {
            reject(err);
          });
      } else {
        sql
          .orderBy(sort_by, order_by)
          .then(result => {
            resolve(result);
          })
          .catch(err => {
            reject(err);
          });
      }
    }),
    new Promise((resolve, reject) => {
      if (pagination === "true") {
        sql_count
          .clearSelect()
          .clearOrder()
          .count(sort_by)
          .then(res => {
            resolve({
              page: page_number,
              total_page: Math.ceil(parseInt(res[0].count) / limit),
              filtered_record:
                limit < parseInt(res[0].count) ? limit : parseInt(res[0].count),
              total_record: parseInt(res[0].count),
              order_by: order_by,
              sort_by: sort_by
            });
          })
          .catch(err => {
            reject(err);
          });
      } else {
        sql_count
          .clearSelect()
          .clearOrder()
          .count(sort_by)
          .then(res => {
            resolve({
              page: 1,
              total_page: 1,
              filtered_record: parseInt(res[0].count),
              total_record: parseInt(res[0].count),
              order_by: order_by,
              sort_by: sort_by
            });
          })
          .catch(err => {
            reject(err);
          });
      }
    })
  ]);
  return {
    data: result,
    page_info: count
  };
};

module.exports = {
  Pagination
};
